## Application

This web application created using React JS with react hooks.
To replace `react-redux` dependencies, this app using `useReducer` as state management from `react-hooks` as build in features from React.

## Instruction

To run, should [install Node](https://nodejs.org/en/download/) first.
In terminal, use command `npm install` to download dependencies.

## Auth Page

![Auth Page](/public/documentation/1.PNG)
To be able to login, input email as **admin@admin.com** and password **admin**
There is switch component to switch to forget password page

## Forget Password Page

![Forget Password Page](/public/documentation/2.PNG)
To reset password, input email as **admin@admin.com** and will recieve return as `alert` showing the **password**

## Dashboard Page

![Dashboard Page](/public/documentation/3.PNG)
Unfotunately, client side can not recieve any `metadata` from `https://pro-api.coinmarketcap.com/v1/cryptocurrency/listings/latest` end-point and recieve `opaque` type response in `console.log`.

## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.
